<?php
require ('config.php');
if (!$_SESSION['UtilisateurCourant'] -> _id){
    header('Location: index.php');
}
Setaddcv($dbh);

require ('includes/headerPageDeModif.php');

?>
    <link rel="stylesheet" href="css/ajoutercv.css">
    <form id="CVAjout" method="post">
        <h2>Ajouter un poste</h2>
        <div class="miform">
            <label for="poste">Intitulé du poste:</label>
            <input type="text" id="poste" name="poste" placeholder="Poste">
        </div>
        <br>
        <div class="miform" id="selecttype">
            <label for="type">type:</label>
            <select name="type" id="type">
                <option value="">Choississez une option</option>
                <option value="cdi">CDI</option>
                <option value="cdd">CDD</option>
                <option value="alternance">Alternance</option>
                <option value="stage">Stage</option>
                <option value="autre">Autre</option>
            </select>
        </div>
        <br>
        <div class="miform">
            <label for="entreprise">Entreprise:</label>
            <input type="text" id="entreprise" name="entreprise" placeholder="Entreprise">
        </div>
        <br>
        <div class="miform">
            <label for="lieu">Lieu:</label>
            <input type="text" id="lieu" name="lieu" placeholder="Lieu">
        </div>
        <br>
        <div>
            <label for="debutjobdate">Date de début:</label>
            <input type="date" id="debutjobdate" name="debutjobdate">
        </div>
        <br>
        <div class="checkboxposte">
            <label id="actually" for="actuellement">J'occupe actuellement ce poste?</label>
            <input type="checkbox" id="actuellement" name="actuellement">
        </div>
        <br>
        <div>
            <label style="vertical-align: top" for="activites">Activités:</label>
            <textarea name="activites" id="activites" cols="30" rows="5"></textarea>
        </div>
        <br>
        <input type="submit" class="soumettrecv" id="soumettreaddcv" name="soumettreaddcv" value="Soumettre">
    </form>

    <div style="color: red; margin: 100px">
        <?php
        if (isset($msg)) {
            echo $msg;
        }
        ?>
    </div>

<?php
require ('includes/footer.php');
?>