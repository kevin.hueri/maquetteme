<?php
require ('config.php');

if (!$_SESSION['UtilisateurCourant'] -> _id){
    header('Location: index.php');}

//ajout ou suppression d ami
if (!empty($_POST)) {
    extract($_POST);
    $valid = (boolean)true;
    if (isset($_POST['userAjouter'])) {
        $req = $dbh->prepare("SELECT * FROM followers WHERE (userid = ? AND userid2 = ?) 
                           OR (userid2 = ? AND userid =?) ");
        $req->execute(array($_SESSION['UtilisateurCourant']->_id, $_GET['id'], $_GET['id'], $_SESSION['UtilisateurCourant']->_id));
        $verifRelation = $req->fetch();
        if (isset($verifRelation['id'])) {
            $valid = false;
        }
        if ($valid) {
            $req = $dbh->prepare("INSERT INTO followers (userid, userid2, status) VALUES (?,?,?)");
            $req->execute(array($_SESSION['UtilisateurCourant']->_id, $_GET['id'], 1));
        }
        header('Location: profilvisiteur.php?id=' . $_GET['id']);
        exit;
    } elseif (isset($_POST['userSupprimer'])) {
        $req = $dbh->prepare("DELETE FROM followers WHERE (userid = ? AND userid2 = ?) 
                           OR (userid2 = ? AND userid =?)");
        $req->execute(array($_SESSION['UtilisateurCourant']->_id, $_GET['id'], $_GET['id'], $_SESSION['UtilisateurCourant']->_id));
        header('Location: profilvisiteur.php?id=' . $_GET['id']);
        exit;
    }
}

//Récupération des informations
if (isset($_SESSION['UtilisateurCourant']->_id)) {
    $req = $dbh->prepare("SELECT *FROM login 
                            LEFT JOIN followers ON (followers.userid = login.id AND followers.userid2 = :id1)
                            OR (followers.userid = :id1 AND followers.userid2 = login.id)WHERE login.id = :id2");
    $req->execute(array('id1' => $_GET['id'], 'id2' => $_SESSION['UtilisateurCourant']->_id));
} else {
    $req = $dbh->prepare("SELECT *FROM login WHERE login.id = :id2");
    $req->execute(array('id2' => $_SESSION['UtilisateurCourant']->_id));
}
$req = $req->fetch();

require('includes/headerPageDeModif.php');

    //Demande d'ami
if (isset($_SESSION['UtilisateurCourant']->_id)){
?>
    <form id="demandeAmi" method="post">
        <?php
        if (!isset($req['status'])){
            ?>
            <input id="ajouterAmi" type="submit" name="userAjouter" value="Ajouter">
            <?php
        } elseif (isset($req['status'])
            && $req['userid'] == $_SESSION['UtilisateurCourant']->_id
            && $req['status'] <> 2) {
            ?>
            <div id="demandeEnAttente">Demande en attente</div>
            <?php
        } elseif (isset($req['status']) && $req['userid2'] == $_SESSION['UtilisateurCourant']->_id && $req['status'] <> 2) {
            ?>
            <div id="demandeEnAttente">Vous avez une demande a accepter</div>
            <?php
        } elseif (isset($req['status']) && $req['status'] == 2) {
            ?>
            <div id="demandeEnAttente">Vous êtes amis</div>
            <?php
        }
        if (isset($req['status']) && $req['userid'] == $_SESSION['UtilisateurCourant']->_id && $req['status'] <> 2){
            ?>
            <input type="submit" id="supprimerDemande" name="userSupprimer" value="Supprimer">
            <?php
        }
        ?>
    </form>
    <?php
}
?>

<div class="infos">
    <?php
    $info = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
    $info -> execute(array($_GET['id']));
    $information = $info->fetch();
    $reqpicture = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
    $reqpicture -> execute(array($_GET['id']));
    $reqpicture = $reqpicture->fetch();
    ?>
    <h2><?php echo $information['prenom']?> <?php echo $information['nom']?></h2>
    <br>
    <p><?php echo $information['emploi']; ?></p>
    <p><?php echo $information['ville']?>, <?php echo $information['departement']?>, <?php echo $information['pays']?></p>
    <div id="pictureVisiteur"><img src="profilpicture/<?php echo $reqpicture['avatar']?>"></div>
</div>


<div class="cv">
    <h2>Expériences</h2>
    <div id="addcv">
        <div class="exps">
            <?php
            $reqexp = $dbh->prepare("SELECT * FROM job WHERE userid = ?");
            $reqexp -> execute(array($_GET['id']));
            $resultexp = $reqexp -> fetchAll();
            foreach ($resultexp as $exp) {
                ?>
                <div class="exp">
                    <a href="#"><img src="maquettes/Crayon-icon.svg" alt="modifier"></a>
                    <h3><?php echo $exp['poste']?></h3>
                    <p>(<?php echo $exp['lieu'] ?>)</p>
                    <p><?php echo $exp['entreprise']?> <?php echo $exp['lieu']?></p>
                    <p>Date de début: <?php echo $exp['datededebut']?></p>
                    <p>Activités: <?php echo $exp['activites']?></p>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>


<div class="socialLink">
    <?php
    $reqlink = $dbh->prepare("SELECT * FROM sociallink WHERE userid = ?");
    $reqlink->execute(array($_GET['id']));
    $reqlink = $reqlink->fetch();
    ?>
    <h2>Réseaux sociaux</h2>
    <div id="link">
        <?php
        if (isset($reqlink['facebook'])){
        ?>
        <a href="<?php echo $reqlink['facebook']?>"><img src="maquettes/facebook-f.svg" alt="facebook"></a>
        <?php
        }
        if (isset($reqlink['linkedin'])){
        ?>
        <a href="<?php echo $reqlink['linkedin']?>"><img src="maquettes/linkedin-in.svg" alt="linkedin"></a>
        <?php
        }
        if (isset($reqlink['tweeter'])){
        ?>
        <a href="<?php echo $reqlink['tweeter']?>"><img src="maquettes/twitter.svg" alt="twitter"></a>
        <?php
        }
        if (isset($reqlink['instagram'])){
        ?>
        <a href="<?php echo $reqlink['instagram']?>"><img src="maquettes/instagram.svg" alt="instagram"></a>
        <?php
        }
        if (isset($reqlink['gitlab'])){
        ?>
        <a href="<?php echo $reqlink['gitlab']?>"><img src="maquettes/gitlab.svg" alt="gitlab"></a>
        <?php
        }
        ?>
    </div>
</div>