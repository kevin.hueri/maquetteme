<?php

//$dsn = "mysql:host=localhost;dbname=lvtx7997_maquetteme;port=3306;charset=utf8";
//$username = "lvtx7997_kevin";
//$password = "Gaby2105!mysql";

//Informations que l on envoie pour la connection a la base de donnée
$dsn = "mysql:host=localhost;dbname=maquetteme;port=3306;charset=utf8";
$username = "eleve";
$password = "bonjour";

try {
//    On se connecte à MariaDB
    $dbh = new PDO($dsn, $username, $password );
}
//En cas d'erreur, on l'affiche et on arrete tout
catch (PDOException $e){
    var_dump($e);
}
session_start();

class Utilisateur
{
    var $_id;
    var $_userName;
    var $_email;

    function
    __construct($_id, $_userName, $_email)
    {
        $this->_email = $_email;
        $this->_userName = $_userName;
        $this->_id = $_id;

    }
}

function SetUserInfos($userinfo)
{
    $_SESSION['UtilisateurCourant'] =
        new Utilisateur($userinfo['id'], $userinfo['username'], $userinfo['email']);
}

function SetFollowers($dbh)
{
    $requserfollow = $dbh->prepare("select userid2,login.username, userstatus.nom 
                                    from followers,login, userstatus where userid = ? 
                                    and followers.userid2 = login.id 
                                    and userstatus.id = login.idstatus ");
    $requserfollow->execute(array($_SESSION['UtilisateurCourant']->_id));
    $follow = $requserfollow->fetchAll();
    $_SESSION['followers'] = $follow;
}

function SetDroits($dbh, $userinfo)
{
    $reqdroituser = $dbh->prepare("SELECT typelogin.id, typelogin.nom FROM typelogin, usertype, login WHERE typelogin.id = usertype.typeid AND login.id = usertype.userid AND login.id = ?");
    $reqdroituser->execute(array($userinfo['id']));
    if ($reqdroituser->rowCount() == 0) {
        $inserttype = $dbh->prepare("INSERT INTO usertype(userid, typeid) VALUES (?, ?)");
        $inserttype->execute(array($_SESSION['UtilisateurCourant']->_id, 2));
        $reqdroituser = $dbh->prepare("SELECT typelogin.id, typelogin.nom FROM typelogin, usertype, login WHERE typelogin.id = usertype.typeid AND login.id = usertype.userid AND login.id = ?");
        $reqdroituser->execute(array($userinfo['id']));
    }
    $tableauDroits = $reqdroituser->fetchAll();
    $_SESSION['droits'] = $tableauDroits;
}

function SetPost($dbh)
{
    if (isset($_POST['post'])) {
        $postit = $_POST['post'];
        $reqpostit = $dbh->prepare("INSERT INTO `actus`(`userid`, `post`) VALUES (?,?) ");
        $reqpostit->execute(array($_SESSION['UtilisateurCourant']->_id, $postit));
        header("Location: profil.php?id=".$_SESSION['UtilisateurCourant']->_id);
    }
}

function SetInsertInfoId($dbh)
{
    $reqiduser = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
    $reqiduser->execute(array($_SESSION['UtilisateurCourant']->_id));
    if ($reqiduser->rowCount() == 0) {
        $insertiduser = $dbh->prepare("INSERT INTO `infosuser`(`iduser`) VALUES (?)");
        $insertiduser->execute(array($_SESSION['UtilisateurCourant']->_id));
    }
}

function SetInsertProject($dbh) {
    if (isset($_POST['projectValid'])) {
        $titre = htmlspecialchars($_POST['titre']);
        $description = htmlspecialchars($_POST['description']);
        $git = htmlspecialchars($_POST['gitproject']);
        if (!empty($_FILES['imageproject']) and !empty($titre) and !empty($description) and !empty($git)) {
            $tailleMax = 2097152;
            if ($_FILES['imageproject']['size'] <= $tailleMax) {
                $extensionsValides = array('jpg', 'jpeg', 'gif', 'png');
                $extensionUpload = strtolower(substr(strrchr($_FILES['imageproject']['name'], "."), 1));
                if (in_array($extensionUpload, $extensionsValides)) {
                    $myUploadedFile = $_FILES["imageproject"];
                    $tmpName = $myUploadedFile["tmp_name"];
                    $fileName = time() . '_' . $myUploadedFile['name'];
                    $resultat = move_uploaded_file($tmpName, "projectpicture/" . $fileName);
                    if ($resultat) {
                        $updateavatar = $dbh->prepare('INSERT INTO `projets`(`userid`, `titre`, `description`, `image`, `git`) VALUES (?,?,?,?,?)');
                        $updateavatar->execute(array($_SESSION['UtilisateurCourant']->_id, $titre, $description, $fileName, $git));
                        header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
                    } else {
                        $msg = "Erreur durant l'importation de votre photo de profil!";
                    }
                } else {
                    $erreur = "Votre photo de profil doit être au format jpg, jpeg, gif ou png!";
                }
            } else {
                $erreur = "Votre photo de profil ne doit pas dépasser 2Mo!";
            }
        } else {
            $erreur = "Tous les champs doivent être remplis";
        }
    }
}

function SetPrenom($dbh)
{
    if (isset($_POST['formmodifprofil'])) {
        if (!empty($_POST['prenom'])) {
            $viewinfos = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
            $viewinfos->execute(array($_SESSION['UtilisateurCourant']->_id));
            $infosexist = $viewinfos->rowCount();
            if ($infosexist == 0) {
                $insert_info_user = $dbh->prepare("INSERT INTO infosuser(iduser, prenom) VALUES (?, ?)");
                $insert_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['prenom']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_info_user = $dbh->prepare("UPDATE `infosuser` SET iduser=?,prenom=? WHERE iduser=?");
                $update_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['prenom'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function SetNom($dbh)
{
    if (isset($_POST['formmodifprofil'])) {
        if (!empty($_POST['nom'])) {
            $viewinfos = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
            $viewinfos->execute(array($_SESSION['UtilisateurCourant']->_id));
            $infosexist = $viewinfos->rowCount();
            if ($infosexist == 0) {
                $insert_info_user = $dbh->prepare("INSERT INTO infosuser(iduser, nom) VALUES (?, ?)");
                $insert_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['nom']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_info_user = $dbh->prepare("UPDATE `infosuser` SET iduser=?,nom=? WHERE iduser=?");
                $update_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['nom'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function SetVille($dbh)
{
    if (isset($_POST['formmodifprofil'])) {
        if (!empty($_POST['ville'])) {
            $viewinfos = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
            $viewinfos->execute(array($_SESSION['UtilisateurCourant']->_id));
            $infosexist = $viewinfos->rowCount();
            if ($infosexist == 0) {
                $insert_info_user = $dbh->prepare("INSERT INTO infosuser(iduser, ville) VALUES (?, ?)");
                $insert_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['ville']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_info_user = $dbh->prepare("UPDATE `infosuser` SET iduser=?,ville=? WHERE iduser=?");
                $update_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['ville'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function SetPays($dbh)
{
    if (isset($_POST['formmodifprofil'])) {
        if (!empty($_POST['pays'])) {
            $viewinfos = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
            $viewinfos->execute(array($_SESSION['UtilisateurCourant']->_id));
            $infosexist = $viewinfos->rowCount();
            if ($infosexist == 0) {
                $insert_info_user = $dbh->prepare("INSERT INTO infosuser(iduser, pays) VALUES (?, ?)");
                $insert_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['pays']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_info_user = $dbh->prepare("UPDATE `infosuser` SET iduser=?,pays=? WHERE iduser=?");
                $update_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['pays'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function SetDepartement($dbh)
{
    if (isset($_POST['formmodifprofil'])) {
        if (!empty($_POST['departement'])) {
            $viewinfos = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
            $viewinfos->execute(array($_SESSION['UtilisateurCourant']->_id));
            $infosexist = $viewinfos->rowCount();
            if ($infosexist == 0) {
                $insert_info_user = $dbh->prepare("INSERT INTO infosuser(iduser, departement) VALUES (?, ?)");
                $insert_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['departement']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_info_user = $dbh->prepare("UPDATE `infosuser` SET iduser=?,departement=? WHERE iduser=?");
                $update_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['departement'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function Setemploi($dbh)
{
    if (isset($_POST['formmodifprofil'])) {
        if (!empty($_POST['emploi'])) {
            $viewinfos = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
            $viewinfos->execute(array($_SESSION['UtilisateurCourant']->_id));
            $infosexist = $viewinfos->rowCount();
            if ($infosexist == 0) {
                $insert_info_user = $dbh->prepare("INSERT INTO infosuser(iduser, emploi) VALUES (?, ?)");
                $insert_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['emploi']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_info_user = $dbh->prepare("UPDATE `infosuser` SET iduser=?,emploi=? WHERE iduser=?");
                $update_info_user->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['emploi'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function SetPicture($dbh)
{
    if (isset($_POST['formmodifprofil'])) {
        if (isset($_FILES['photodeprofil'])) {
            $tailleMax = 2097152;
            $extensionsValides = array('jpg', 'jpeg', 'gif', 'png');
            if ($_FILES['photodeprofil']['size'] <= $tailleMax) {
                $extensionUpload = strtolower(substr(strrchr($_FILES['photodeprofil']['name'], "."), 1));
                if (in_array($extensionUpload, $extensionsValides)) {
                    $myUploadedFile = $_FILES["photodeprofil"];
                    $tmpName = $myUploadedFile["tmp_name"];
                    $fileName = time() . '_' . $myUploadedFile['name'];
                    $resultat = move_uploaded_file($tmpName, "profilpicture/" . $fileName);
                    if ($resultat) {
                        $updateavatar = $dbh->prepare('UPDATE infosuser SET avatar = ? WHERE iduser = ?');
                        $updateavatar->execute(array($fileName, $_SESSION['UtilisateurCourant']->_id));
                        header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
                    } else {
                        $msg = "Erreur durant l'importation de votre photo de profil!";
                    }
                } else {
                    $msg = "Votre photo de profil doit être au format jpg, jpeg, gif ou png!";
                }
            } else {
                $msg = "Votre photo de profil ne doit pas dépasser 2Mo!";
            }
        }
    }
}

function Setaddcv($dbh)
{
    if (isset($_POST['soumettreaddcv'])) {
        $poste = htmlspecialchars($_POST['poste']);
        $type = htmlspecialchars($_POST['type']);
        $entreprise = htmlspecialchars($_POST['entreprise']);
        $lieu = htmlspecialchars($_POST['lieu']);
        $activites = htmlspecialchars($_POST['activites']);
        if (!empty($poste) and !empty($type) and !empty($entreprise) and !empty($lieu) and !empty($activites) and
            !empty($_POST['debutjobdate'])) {
            if (isset($_POST['actuellement'])) {
                $occupationactuelle = "oui";
            } else {
                $occupationactuelle = "non";
            }
            $insertjob = $dbh->prepare("INSERT INTO job(userid, poste, type, entreprise, lieu,
                             occupationactuelle, datededebut, activites) VALUES (?,?,?,?,?,?,?,?)");
            $insertjob->execute(array($_SESSION['UtilisateurCourant']->_id, $poste, $type, $entreprise, $lieu, $occupationactuelle,
                $_POST['debutjobdate'], $activites));
            header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
        } else {
            $msg = "Tout les champs doivent être remplis!";
        }
    }
}

function SetTheme($dbh)
{
    if (isset($_POST['formmodifprofil'])) {
        $reqtheme = $dbh->prepare("UPDATE `infosuser` SET `theme`=? WHERE iduser = ?");
        $reqtheme->execute(array($_POST["couleur"], $_SESSION['UtilisateurCourant']->_id));
        header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
    }
}

function SetFacebook($dbh)
{
    if (isset($_POST['formmodifsociallink'])) {
        if (!empty($_POST['facebook'])) {
            $reqfacebook = $dbh->prepare("SELECT * FROM sociallink WHERE userid = ?");
            $reqfacebook->execute(array($_SESSION['UtilisateurCourant']->_id));
            $reqfacebook = $reqfacebook->rowCount();
            if ($reqfacebook == 0) {
                $insert_facebook = $dbh->prepare("INSERT INTO sociallink(userid, facebook) VALUES (?, ?)");
                $insert_facebook->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['facebook']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_facebook = $dbh->prepare("UPDATE `sociallink` SET facebook=? WHERE userid=?");
                $update_facebook->execute(array($_POST['facebook'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function SetLinkedin($dbh)
{
    if (isset($_POST['formmodifsociallink'])) {
        if (!empty($_POST['linkedin'])) {
            $reqlinkedin = $dbh->prepare("SELECT * FROM sociallink WHERE userid = ?");
            $reqlinkedin->execute(array($_SESSION['UtilisateurCourant']->_id));
            $reqlinkedin = $reqlinkedin->rowCount();
            if ($reqlinkedin == 0) {
                $insert_linkedin = $dbh->prepare("INSERT INTO sociallink(userid, linkedin) VALUES (?, ?)");
                $insert_linkedin->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['linkedin']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_linkedin = $dbh->prepare("UPDATE `sociallink` SET linkedin=? WHERE userid=?");
                $update_linkedin->execute(array($_POST['linkedin'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function SetTweeter($dbh)
{
    if (isset($_POST['formmodifsociallink'])) {
        if (!empty($_POST['tweeter'])) {
            $reqtweeter = $dbh->prepare("SELECT * FROM sociallink WHERE userid = ?");
            $reqtweeter->execute(array($_SESSION['UtilisateurCourant']->_id));
            $reqtweeter = $reqtweeter->rowCount();
            if ($reqtweeter == 0) {
                $insert_tweeter = $dbh->prepare("INSERT INTO sociallink(userid, tweeter) VALUES (?, ?)");
                $insert_tweeter->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['tweeter']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_tweeter = $dbh->prepare("UPDATE `sociallink` SET tweeter=? WHERE userid=?");
                $update_tweeter->execute(array($_POST['tweeter'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function SetInstagram($dbh)
{
    if (isset($_POST['formmodifsociallink'])) {
        if (!empty($_POST['instagram'])) {
            $reqinstagram = $dbh->prepare("SELECT * FROM sociallink WHERE userid = ?");
            $reqinstagram->execute(array($_SESSION['UtilisateurCourant']->_id));
            $reqinstagram = $reqinstagram->rowCount();
            if ($reqinstagram == 0) {
                $insert_instagram = $dbh->prepare("INSERT INTO sociallink(userid, instagram) VALUES (?, ?)");
                $insert_instagram->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['instagram']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_tweeter = $dbh->prepare("UPDATE `sociallink` SET instagram=? WHERE userid=?");
                $update_tweeter->execute(array($_POST['instagram'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

function SetGitlab($dbh)
{
    if (isset($_POST['formmodifsociallink'])) {
        if (!empty($_POST['gitlab'])) {
            $reqgitlab = $dbh->prepare("SELECT * FROM sociallink WHERE userid = ?");
            $reqgitlab->execute(array($_SESSION['UtilisateurCourant']->_id));
            $reqgitlab = $reqgitlab->rowCount();
            if ($reqgitlab == 0) {
                $insert_gitlab = $dbh->prepare("INSERT INTO sociallink(userid, gitlab) VALUES (?, ?)");
                $insert_gitlab->execute(array($_SESSION['UtilisateurCourant']->_id, $_POST['gitlab']));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            } else {
                $update_gitlab = $dbh->prepare("UPDATE `sociallink` SET gitlab=? WHERE userid=?");
                $update_gitlab->execute(array($_POST['gitlab'], $_SESSION['UtilisateurCourant']->_id));
                header("Location: profil.php?id=" . $_SESSION['UtilisateurCourant']->_id);
            }
        }
    }
}

