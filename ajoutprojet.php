<?php
require ('config.php');
SetInsertProject($dbh);
if (!$_SESSION['UtilisateurCourant'] -> _id){
    header('Location: index.php');}
require ('includes/headerPageDeModif.php');
$erreur ="";
?>
    <link rel="stylesheet" href="css/style-ajoutprojet.css">

    <form id="projetAjout" action="" method="post" enctype="multipart/form-data">
        <h2>Ajout d'un projet</h2>
        <div id="firtDivProject">
            <label for="titre">Titre du projet:</label>
            <input type="text" id="titre" name="titre" placeholder="Titre">
        </div>
        <br>
        <div>
            <label for="description">Description du projet:</label>
            <input type="text" id="description" name="description" placeholder="Description">
        </div>
        <br>
        <div>
            <label for="gitproject">Lien Git: </label>
            <input type="text" id="gitproject" name="gitproject" placeholder="Git">
        </div>
        <br>
        <div class="imageprojetDiv">
            <label id="imageProjectLabel" for="imageproject">Image représentant le projet:</label>
            <input type="file" id="imageproject" name="imageproject">
        </div>
        <br>
        <input type="submit" id="enregistrer" name="projectValid" value="enregistrer">
    </form>
    <div>
    <?php
    if (isset($erreur)) {
        echo $erreur;
        }
    ?>
    </div>
<?php
require ('includes/footer.php')
?>