<?php
require('config.php');

if (isset($_POST['forminscription'])) {
    $pseudo = htmlspecialchars($_POST['username']);
    $mail = htmlspecialchars($_POST['email']);
    //TODO: remplacer sha1 par password_hash
    //https://www.php.net/manual/fr/function.password-hash.php
    $password = sha1($_POST['password']);
    $password2 = sha1($_POST['password2']);
    if (!empty($_POST['username']) AND !empty($_POST['email']) AND
        !empty($_POST['password']) AND !empty($_POST['password2'])) {
        $pseudolengh = strlen($pseudo);
        if ($pseudolengh <= 255) {
            $reqpseudo = $dbh -> prepare("SELECT * FROM login WHERE username = ?");
            $reqpseudo -> execute(array($pseudo));
            $pseudoexist = $reqpseudo->rowCount();
            if ($pseudoexist == 0) {
                if (filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                    $reqmail = $dbh->prepare("SELECT * FROM login WHERE email = ?");
                    $reqmail->execute(array($mail));
                    $mailexist = $reqmail->rowCount();
                    if ($mailexist == 0) {
                        if ($password == $password2) {
                            $insertmbr = $dbh->prepare("INSERT INTO 
                                            login(username, email, password, reactifdate)
                                             VALUES (?, ?, ?, DEFAULT)");
                            $insertmbr->execute(array($pseudo, $mail, $password));
                            $_SESSION['comptecree'] = "Votre compte à bien été créé !";
                            header('Location: index.php');
                        } else {
                            $erreur = "Vos mots de passe ne correspondent pas!";
                        }
                    } else {
                        $erreur = "Adresse mail déjà utilisée!";
                    }
                } else {
                    $erreur = "Votre adresse mail n'est pas valide!";
                }
            } else {
                $erreur = "Identifiant déjà utilisé!";
            }
        } else {
            $erreur = "Votre identifiant ne doit pas dépasser 255 caractères!";
        }

    } else {
        $erreur = "Tous les champs doivent être complétés!";
    }
}
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inscription</title>
    <link rel="stylesheet" href="css/style_inscription.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Indie+Flower&display=swap">
</head>
    <body>
        <div class="sub-header">
            <p class="en-tete">Adoptun<b>Dev</b></p>
            <h1>Inscription</h1>
            <form action="" method="post">

                <tr>
                    <td>
                        <label for="username">Identifiant:</label>
                    </td>
                    <td>
                        <input type="text" id="username" name="username" placeholder="Votre pseudo"
                               value="<?php if (isset($pseudo)) {echo $pseudo;} ?>">
                    </td>

                    <td>
                        <label for="email">Email:</label>
                    </td>
                    <td>
                        <input type="email" id="email" name="email" placeholder="Votre mail" value="<?php if (isset($mail)) {echo $mail;} ?>">
                    </td>

                    <td>
                        <label for="password">Mot de passe:</label>
                    </td>
                    <td>
                        <input type="password" id="password" name="password" placeholder="Votre mot de passe">
                    </td>

                    <td>
                        <label for="password2">Confirmation du mot de passe:</label>
                    </td>
                    <td>
                        <input type="password" id="password2" name="password2" placeholder="Confirmation">
                    </td>

                    <td>
                        <input class="inscription_button" type="submit" name="forminscription" value="inscription">
                    </td>
                </tr>

            </form>
            <div class="erreur_inscription">
            <?php
            if (isset($erreur)) {
                echo $erreur;
            }
            ?>
            </div>
        </div>
    </body>
</html>
