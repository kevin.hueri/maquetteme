    // Barre de navigation
    jQuery(function ($) {
        var anchor = window.location.hash;
        $('.tabs').each(function () {
            var current = null;
            var id = $(this).attr('id');
            if (anchor != '' && $(this).find('a[href="' + anchor + '"]').length > 0){
                current = anchor;
            }else if ($.cookie('tab'+id) && $(this).find('a[href="' + $.cookie('tab'+id) + '"]').length > 0)
            {
                current = $.cookie('tab'+id);
            }else {
                current = $(this).find('a:first').attr('href');
            }
            $(this).find('a[href="' + current + '"]').addClass('active');
            $(current).siblings().hide();
            $(this).find('a').click(function () {
                var link = $(this).attr('href');
                if (link == current) {
                    return false;
                } else {
                    // $(this).siblings().removeClass('active');
                    $(this).addClass('active');
                    $(link).show().siblings().hide();
                    current = link;
                    $.cookie('tab'+id,current);
                }
            });
        });
    });


// Moteur de recherche
    $(document).ready(function (){
        $('#rechercheAmi').keyup(function (){
            $('#result-search').html('')
            var utilisateur = $(this).val();
            if (utilisateur != ""){
                $.ajax({
                    type: 'GET',
                    url: 'fonctions/recherche_utilisateur.php',
                    data: 'user=' + encodeURIComponent(utilisateur),
                    success: function (data){
                        if (data != ""){
                            $('#result-search').append(data);
                        }else{
                            document.getElementById('result-search')
                                .innerHTML = "<div style= 'font-size: 14px; text-align: center;" +
                                " margin-top: 10px'>Aucun utilisateur</div>"
                        }
                    }
                });
            }
        })
    })