<?php
require ('config.php');

if (isset($_POST['formconnexion'])) {
    $pseudoconnect = htmlspecialchars($_POST['pseudoconnect']);
    $mdpconnect = sha1($_POST['mdpconnect']);
    if (!empty($pseudoconnect) AND !empty($mdpconnect)) {
        $requser = $dbh -> prepare("SELECT * FROM login WHERE username = ? 
                      AND password = ?");
        $requser->execute(array($pseudoconnect, $mdpconnect));
        $userexist = $requser->rowCount();
        if ($userexist == 1) {
            $userinfo = $requser->fetch();
//            informations de l'utilisateur
            SetUserInfos($userinfo);
//            Droits
            SetDroits($dbh,$userinfo);
//            Followers
            SetFollowers($dbh);
//            Edition profil
            SetInsertInfoId($dbh);
//            Redirection vers ma page de profil
            header("Location: profil.php?id=".$_SESSION['UtilisateurCourant']->_id);
        } else {
            $erreur = "Mauvais identifiant ou mot de passe !";
        }
    } else {
        $erreur = "Tous les champs doivent être complétés !";
    }
}
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acceuil</title>
    <link rel="stylesheet" href="css/style_index.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Indie+Flower&display=swap">
</head>
<body>
    <div class="index">
        <p class="en-tete">Adoptun<b>Dev</b></p>
        <h1>Identification</h1>
        <div id="login">
            <form method="post">
                <label id="identifiant" for="pseudoconnect">Identifiant:</label>
                <input type="text" id="pseudoconnect" name="pseudoconnect">
                <br><br>
                <label for="mdpconnect">Mot de passe:</label>
                <input type="password" id="mdpconnect" name="mdpconnect">
                <br><br>
                <input id="connexion" type="submit" name="formconnexion" value="Connexion" style="width: 150px; text-align: center">
                <a class="inscription" href="inscription.php">S'inscrire</a>
            </form>
            <div class="erreur_login">
                <?php
                if (isset($erreur)) {
                    echo $erreur;
                }
                ?>
            </div>
        </div>
    </div>
</body>
</html>