<link rel="stylesheet" href="css/style_informations.css">

<div class="infos">
    <div id="modifbutton">
        <a id="modif" href="modifierprofil.php"><img src="maquettes/Crayon-icon.svg" alt="modifier"></a>
    </div>
    <?php
    $info = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
    $info -> execute(array($_SESSION['UtilisateurCourant']->_id));
    $information = $info->fetch();
    $reqpicture = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
    $reqpicture -> execute(array($_SESSION['UtilisateurCourant']->_id));
    $reqpicture = $reqpicture->fetch();
    ?>
    <h2><?php echo $information['prenom']?> <?php echo $information['nom']?></h2>
    <br>
    <p><?php echo $information['emploi']; ?></p>
    <p><?php echo $information['ville']?>, <?php echo $information['departement']?>, <?php echo $information['pays']?></p>
    <div id="pictureUser"><img src="profilpicture/<?php echo $reqpicture['avatar']?>"></div>


</div>


<div class="cv">
    <a id="addcvlien" href="ajoutercv.php"><img src="maquettes/plus.svg" alt="ajouter"></a>
    <h2>Expériences</h2>
    <div id="addcv">
        <div class="exps">
            <?php
            $reqexp = $dbh->prepare("SELECT * FROM job WHERE userid = ?");
            $reqexp -> execute(array($_SESSION['UtilisateurCourant']->_id));
            $resultexp = $reqexp -> fetchAll();
            foreach ($resultexp as $exp) {
            ?>
            <div class="exp">
                <a href="#"><img src="maquettes/Crayon-icon.svg" alt="modifier"></a>
                <h3><?php echo $exp['poste']?></h3>
                <p>(<?php echo $exp['lieu'] ?>)</p>
                <p><?php echo $exp['entreprise']?> <?php echo $exp['lieu']?></p>
                <p>Date de début: <?php echo $exp['datededebut']?></p>
                <p>Activités: <?php echo $exp['activites']?></p>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>


<div class="socialLink">
    <?php
    $reqlink = $dbh->prepare("SELECT * FROM sociallink WHERE userid = ?");
    $reqlink->execute(array($_SESSION['UtilisateurCourant']->_id));
    $reqlink = $reqlink->fetch();
    ?>
    <a id="modif" href="sociallink.php"><img src="maquettes/Crayon-icon.svg" alt="modifier"></a>
    <h2>Réseaux sociaux</h2>
    <div id="link">
            <a href="
            <?php if (isset($reqlink['facebook'])){
                echo $reqlink['facebook'];
            } else {
                echo "#";
            }?>
            "><img src="maquettes/facebook-f.svg" alt="facebook"></a>
            <a href="
            <?php if (isset($reqlink['linkedin'])){
                echo $reqlink['linkedin'];
            } else {
                echo "#";
            }?>
            "><img src="maquettes/linkedin-in.svg" alt="linkedin"></a>
            <a href="
            <?php if (isset($reqlink['tweeter'])){
                echo $reqlink['tweeter'];
            } else {
                echo "#";
            }?>
            "><img src="maquettes/twitter.svg" alt="twitter"></a>
            <a href="
            <?php if (isset($reqlink['instagram'])){
                echo $reqlink['instagram'];
            } else {
                echo "#";
            }?>
            "><img src="maquettes/instagram.svg" alt="instagram"></a>
            <a href="
            <?php if (isset($reqlink['gitlab'])){
                echo $reqlink['gitlab'];
            } else {
                echo "#";
            }?>
            "><img src="maquettes/gitlab.svg" alt="gitlab"></a>
    </div>
</div>