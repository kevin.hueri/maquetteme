<?php
//Couleur du Theme
$reqthemechoice = $dbh->prepare("SELECT theme FROM infosuser WHERE iduser = ?");
$reqthemechoice->execute(array($_SESSION['UtilisateurCourant']->_id));
$reqthemechoice = $reqthemechoice->fetch();

//Récupération des informations
$reqnotif = $dbh->prepare("SELECT f.id, l.username, l.id id_utilisateur
FROM `followers` f INNER JOIN login l ON l.id = f.userid WHERE f.userid2 = ?
AND f.status = ?");
$reqnotif->execute(array($_SESSION['UtilisateurCourant']->_id, 1));
$afficherDemandes = $reqnotif -> rowCount();

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acceuil</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../cookie/jquery.cookie.js"></script>
    <link rel="stylesheet" href="css/style_header.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Indie+Flower&display=swap">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/style_infosvisiteur.css">

    <script type="text/javascript" src="main.js"></script>
</head>
<body class="corps">
<main>
    <header>
        <div style="background-color: <?php echo $reqthemechoice['theme'] ?>" id="header" class="sub-header">

            <!--                    Titre-->
            <div class="title">
                <p class="en-tete">Adoptun<b>Dev</b></p>
                <!--                <h1 class="font-alt">Bonjour --><?php //echo $_SESSION['UtilisateurCourant']->_userName; ?><!--</h1>-->
            </div>

            <!--                    deconnexion-->
            <div class="deco">
                <?php
                if (isset($_SESSION['UtilisateurCourant']->_id)){
                    ?>
                    <a class="logout" href="deconnexion.php"><img src="maquettes/logout.svg" alt="LogOut"></a>
                    <?php
                }
                ?>
            </div>

            <!--                    Photo de profil-->
            <div class="profilpicture">
                <?php
                $avatarProfil = $dbh ->prepare("SELECT * FROM infosuser WHERE iduser = ?");
                $avatarProfil->execute(array($_SESSION['UtilisateurCourant']->_id));
                $photo = $avatarProfil ->fetch();
                if (isset($photo['avatar'])) {
                    ?>
                    <img src="../profilpicture/<?php echo $photo['avatar'] ?>">
                    <?php
                }
                ?>
            </div>

            <!--                    Profil de droit-->
            <div class="droitsadmin">
                <?php
                $isAdmin = false;
                foreach ($_SESSION['droits'] as $droit) {
                    if ($droit['nom'] === "admin") {
                        $isAdmin = true;
                    }
                }
                if($isAdmin)
                {
                    ?>
                    <a class="admin" href="#"><img src="maquettes/admin.svg" alt="admin"></a>
                    <?php
                }
                ?>
            </div>

            <!--            Barre de recherche-->
            <div id="moteurRecherche" class="barreDeRecherche">
                <input type="text" id="rechercheAmi" name="recherche" placeholder="rechercher">
                <div class="resultRecherche">
                    <div id="result-search"></div>
                </div>
            </div>

        </div>

        <div class="menu">
            <div class="tabs" id="tabs1">
                <a href="profil.php?id=<?php echo $_SESSION['UtilisateurCourant']->_id?>#contenu1">
                    <img src="maquettes/acceuil.svg" alt="acceuil"></a>
                <a href="profil.php?id=<?php echo $_SESSION['UtilisateurCourant']->_id?>#contenu2">
                    <img src="maquettes/information.svg" alt="informations"></a>
                <a href="profil.php?id=<?php echo $_SESSION['UtilisateurCourant']->_id?>#contenu3">
                    <img src="maquettes/projet.svg" alt="projets"></a>
                <?php
                if ($afficherDemandes == 0){
                    ?>
                    <a href="profil.php?id=<?php echo $_SESSION['UtilisateurCourant']->_id?>#contenu4">
                        <img src="../maquettes/bouton-notifications.svg" alt="notifications"></a>
                    <?php
                } else{
                    ?>
                    <a href="profil.php?id=<?php echo $_SESSION['UtilisateurCourant']->_id?>#contenu4">
                        <img src="../maquettes/bouton-notifications2.svg" alt="notifications">
                    </a>
                    <?php
                }
                ?>

            </div>
        </div>
    </header>
