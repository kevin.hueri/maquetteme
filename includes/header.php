<?php
//Récupération des informations
$req = $dbh->prepare("SELECT f.id, l.username, l.id id_utilisateur
FROM `followers` f INNER JOIN login l ON l.id = f.userid WHERE f.userid2 = ?
AND f.status = ?");
$req->execute(array($_SESSION['UtilisateurCourant']->_id, 1));
$afficherDemandes = $req -> rowCount();
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>En-tete</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="css/style_header.css">
    <link rel="stylesheet" href="css/style_acceuil.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Indie+Flower&display=swap">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
</head>
<body class="corps">

<?php
    $reqthemechoice = $dbh->prepare("SELECT theme FROM infosuser WHERE iduser = ?");
    $reqthemechoice -> execute(array($_SESSION['UtilisateurCourant']->_id));
    $reqthemechoice = $reqthemechoice -> fetch();
?>
<main>
    <header>
        <div style="background-color: <?php echo $reqthemechoice['theme'] ?>" class="sub-header">

            <!--                    Titre-->
            <div class="title">
                <p class="en-tete">Adoptun<b>Dev</b></p>
            </div>

            <!--                    deconnexion-->
            <div class="deco">
                <?php
                if (isset($_SESSION['UtilisateurCourant']->_id)){
                    ?>
                    <a class="logout" href="deconnexion.php">
                        <img src="maquettes/logout.svg" alt="LogOut">
                    </a>
                    <?php
                }
                ?>
            </div>

            <!--                    Photo de profil-->
            <div class="profilpicture">
                <?php
                $avatarProfil = $dbh ->prepare("SELECT avatar FROM infosuser WHERE iduser = ?");
                $avatarProfil->execute(array($_SESSION['UtilisateurCourant']->_id));
                $photo = $avatarProfil ->fetch();
                ?>
                    <img src="profilpicture/<?php echo $photo['avatar']?>">
            </div>

            <!--                    Profil de droit-->
            <div class="droitsadmin">
                <?php
                $isAdmin = false;
                foreach ($_SESSION['droits'] as $droit) {
                    if ($droit['nom'] === "admin") {
                        $isAdmin = true;
                    }
                }
                if($isAdmin)
                {
                    ?>
                    <a class="admin" href="admin/index.php?<?php echo $_SESSION['UtilisateurCourant']->_id ?>"><img src="maquettes/admin.svg" alt="admin"></a>
                    <?php
                }
                ?>
            </div>

<!--            moteur de recherche-->
            <div class="barreDeRecherche">
                <input type="text" id="rechercheAmi" name="recherche" placeholder="rechercher">
                <div id="result-search"></div>
            </div>

        </div>

<!--        Barre de navigation-->
        <div class="menu">
            <div class="tabs" id="tabs1">
                <a href="#contenu1"><img src="maquettes/acceuil.svg" alt="acceuil"></a>
                <a href="#contenu2"><img src="maquettes/information.svg" alt="informations"></a>
                <a href="#contenu3"><img src="maquettes/projet.svg" alt="projets"></a>
                <?php
                if ($afficherDemandes == 0){
                ?>
                <a href="#contenu4">
                    <img src="maquettes/bouton-notifications.svg" alt="notifications"></a>
                <?php
                } else{
                ?>
                <a href="#contenu4">
                    <img src="maquettes/bouton-notifications2.svg" alt="notifications">
                </a>
                <?php
                }
                ?>
            </div>
        </div>
    </header>
