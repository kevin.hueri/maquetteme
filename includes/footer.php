</main>

<link rel="stylesheet" href="css/style_footer.css">

<footer>
    <div class="followerlist">
        <ul>
            <?php
            foreach ($_SESSION['followers'] as $follower) {
                $urlUser = "profil.php?id=".$follower['userid2'];
                ?>
                <li>
                    <a href=<?php echo $urlUser?>>
                        <?php
                        echo $follower['username']." ".$follower['nom']
                        ?>
                    </a>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
</footer>
</body>
</html>
