<link rel="stylesheet" href="css/style_projets.css">
<div id="project">
    <div>
        <a class="addproject" href="ajoutprojet.php"><img src="maquettes/plus.svg" alt="ajouter"></a>
    </div>

    <h2>Projets</h2>


    <div class="publicationprojet">

        <?php
        $reqtheme = $dbh->prepare("SELECT theme FROM infosuser WHERE iduser = ?");
        $reqtheme->execute(array($_SESSION['UtilisateurCourant']->_id));
        $reqtheme = $reqtheme->fetch();
        $reqprojets = $dbh->prepare("SELECT * FROM projets WHERE userid = ?" );
        $reqprojets -> execute(array($_SESSION['UtilisateurCourant']->_id));
        $resultprojets = $reqprojets -> fetchAll();
        foreach ($resultprojets as $projet) {
            ?>
            <div class="divprojet" style="border: 3px solid <?php echo $reqtheme['theme']?>;">
                <div class="imageprojet">
                    <img src="projectpicture/<?php echo $projet['image']?>">
                </div>
                <div class="titre">
                    <a id="projetLink" href="<?php echo $projet['git'] ?>"><?php echo $projet['titre'] ?></a>
                </div>
                <br>
                <div class="description">
                    <?php
                    echo $projet['description']
                    ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>