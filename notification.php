<?php
//Récupération des informations
$req = $dbh->prepare("SELECT f.id, l.username, l.id id_utilisateur
        FROM `followers` f INNER JOIN login l ON l.id = f.userid WHERE f.userid2 = ? 
                                                                   AND f.status = ?");
$req->execute(array($_SESSION['UtilisateurCourant']->_id, 1));
$afficherDemandes = $req -> fetchAll();
?>

<link rel="stylesheet" href="css/style_notification.css">

<div class="container">
    <div class="row">
        <?php
        foreach ($afficherDemandes as $demande){
        ?>
        <div id="caseNotif">
            <p>accepter la demande de: </p>
            <div id="nameDemandeur">
                <?php echo $demande['username'] ?>
            </div>
            <div class="buttonDemande">
                <a id="visuProfil"
                   href="profilvisiteur.php?id=<?php echo $demande['id_utilisateur'] ?>">
                    Voir profil
                </a>
            </div>
            <div class="formDemandeAmi">
                <form action="" method="post">
                    <input type="hidden" name="id_relation" value="<?php echo $demande['id']?>">
                    <input type="submit" id="accepterAmi" name="accepter" value="oui">
                    <input type="submit" id="refuserAmi" name="refuser" value="non">
                </form>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
</div>