<?php
require ('config.php');

SetGitlab($dbh);
SetInstagram($dbh);
SetTweeter($dbh);
SetLinkedin($dbh);
SetFacebook($dbh);

require ('includes/headerPageDeModif.php');
?>
    <link rel="stylesheet" href="css/style_reseauxSociaux.css">


    <form id="modifReseauxSociaux" action="" method="post" enctype="multipart/form-data">
        <h2>modifier vos réseaux sociaux</h2>
        <div class="miform">
            <label for="facebook">Facebook: </label>
            <input type="text" id="facebook" name="facebook" placeholder="facebook"><br>
        </div>
        <div class="miform">
            <label for="linkedin">Linkedin: </label>
            <input type="text" id="linkedin" name="linkedin" placeholder="linkedin"><br>
        </div>
        <div class="miform">
            <label for="instagram">Instagram: </label>
            <input type="text" id="instagram" name="instagram" placeholder="instagram"><br>
        </div>
        <div class="miform">
            <label for="tweeter">Tweeter: </label>
            <input type="text" id="tweeter" name="tweeter" placeholder="tweeter"><br>
        </div>
        <div class="miform">
            <label for="gitlab">Gitlab: </label>
            <input type="text" id="gitlab" name="gitlab" placeholder="gitlab"><br>
        </div>

        <input class="validersociallink" name="formmodifsociallink" type="submit" value="Enregistrer">
    </form>
<?php
require ('includes/footer.php');
?>