<?php
require ('config.php');
//Sécurisation des pages utilisateurs
if (!$_SESSION['UtilisateurCourant'] -> _id){
    header('Location: index.php');
}

//Reponse a la demande d'amis
if (!empty($_POST)){
    extract($_POST);
    $valid = (boolean) true;
    if (isset($_POST['accepter'])){
        $id_relation = (int) $id_relation;
        if ($id_relation>0){
            $req = $dbh->prepare("SELECT id FROM followers WHERE id = ? AND status = 1");
            $req -> execute(array($id_relation));
            $verif_relation = $req->fetch();
            if (!isset($verif_relation['id'])){
                $valid = false;
            }
            if ($valid){
                $req = $dbh -> prepare("UPDATE followers SET status = 2 WHERE id = ? AND userid2 = ?" );
                $req -> execute(array($id_relation, $_SESSION['UtilisateurCourant'] ->_id));
            }
        }
        header("Location: profil.php?id=".$_SESSION['UtilisateurCourant']->_id);
        exit;
    } elseif (isset($_POST['refuser'])){
        $id_relation = (int) $id_relation;
        if ($id_relation>0){
            $req = $dbh->prepare("DELETE FROM followers WHERE id = ? AND userid2 = ?");
            $req -> execute(array($id_relation, $_SESSION['UtilisateurCourant'] ->_id));
        }
        header("Location: profil.php?id=".$_SESSION['UtilisateurCourant']->_id);
        exit;
    }
}
require ('includes/header.php');
?>
            <div class="bloc">
                <div class="content">
                    <div id="contenu1">
                        <?php
                        require ('acceuil.php');
                        ?>
                    </div>
                    <div id="contenu2" class="informations">
                        <?php
                        require ('informations.php');
                        ?>
                    </div>
                    <div id="contenu3" class="projets">
                        <?php
                        require ('projets.php');
                        ?>
                    </div>
                    <div id="contenu4" class="notifs">
                        <?php
                        require ('notification.php');
                        ?>
                    </div>
                </div>
            </div>
    <script type="text/javascript" src="main.js"></script>
    <script type="text/javascript" src="cookie/jquery.cookie.js"></script>
