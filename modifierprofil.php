<?php
require ('config.php');
if (!$_SESSION['UtilisateurCourant'] -> _id){
    header('Location: index.php');
}

SetPrenom($dbh);
SetNom($dbh);
SetVille($dbh);
SetPays($dbh);
Setemploi($dbh);
SetDepartement($dbh);
SetPicture($dbh);
SetTheme($dbh);

require ('includes/headerPageDeModif.php');

$reqinfos = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
$reqinfos -> execute(array($_SESSION['UtilisateurCourant']->_id));
$reqinfos = $reqinfos->fetch();

?>
    <link rel="stylesheet" href="css/style_modifier_profil.css">


    <form id="modifprofil" action="" method="post" enctype="multipart/form-data">
        <h2>Modifier votre profil</h2>
        <div class="miform">
            <label for="prenom">Prénom: </label>
            <input type="text" id="prenom" name="prenom" placeholder="<?php echo $reqinfos['prenom']?>"><br>
        </div>
        <div class="miform">
            <label for="nom">Nom: </label>
            <input type="text" id="nom" name="nom" placeholder="<?php echo $reqinfos['nom']?>"><br>
        </div>
        <div class="formtotal">
            <label for="emploi">Emploi</label>
            <input type="text" id="emploi" name="emploi" placeholder="<?php echo $reqinfos['emploi']?>"><br>
        </div>
        <div class="tierform">
            <label for="pays">Pays: </label>
            <input type="text" id="pays" name="pays" placeholder="<?php echo $reqinfos['pays']?>"><br>
        </div>
        <div class="tierform">
            <label for="ville">Ville: </label>
            <input type="text" id="ville" name="ville" placeholder="<?php echo $reqinfos['ville']?>"><br>
        </div>
        <div class="tierform">
            <label for="departement">Département: </label>
            <input type="text" id="departement" name="departement" placeholder="<?php echo $reqinfos['departement']?>"><br>
        </div>
        <div id="radiotheme">
            <p id="themechoice">Thème: </p>
            <div>
                <input type="radio" id="vert" name="couleur" value="#3eac4d" checked>
                <label id="optionVert" for="vert">V</label>
            </div>
            <div>
                <input type="radio" id="bleu" name="couleur" value="#0b5bb6" checked>
                <label id="optionBleu" for="bleu">B</label>
            </div>
            <div>
                <input type="radio" id="rouge" name="couleur" value="#ed291a" checked>
                <label id="optionRouge" for="rouge">R</label>
            </div>
            <div>
                <input type="radio" id="jaune" name="couleur" value="#ffe838" checked>
                <label id="optionJaune" for="jaune">J</label>
            </div>
        </div>
        <div class="divImageProfilModif">
            <label id="labelPhoto" for="photodeprofil">Modifier photo de profil</label>
            <input type="file" id="photodeprofil" name="photodeprofil"><br>
        </div>
        <input class="validerprofil" name="formmodifprofil" type="submit" value="Enregistrer">
    </form>
