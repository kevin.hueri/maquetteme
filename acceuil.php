<?php
$reqpict = $dbh->prepare("SELECT avatar FROM infosuser WHERE iduser = ?");
$reqpict -> execute(array($_SESSION['UtilisateurCourant']->_id));
$reqpict = $reqpict -> fetch();

// Récupérations des posts utilisateurs et de ses amis
$reqpost = $dbh->prepare("SELECT infosuser.avatar, login.username, actus.post, actus.datecreation 
                            FROM actus, login, infosuser
                            WHERE (actus.userid = ? and login.id = actus.userid AND infosuser.iduser = login.id) 
                            OR
                            (login.id = actus.userid AND infosuser.iduser=login.id and actus.userid in 
                            (select followers.userid2 from followers where followers.userid = ?)) 
                            OR  
                            (login.id = actus.userid AND infosuser.iduser=login.id and actus.userid in 
                            (select followers.userid from followers where followers.userid2 = ?))
                            ORDER BY actus.datecreation DESC LIMIT 20" );
$reqpost -> execute(array($_SESSION['UtilisateurCourant']->_id,$_SESSION['UtilisateurCourant']->_id, $_SESSION['UtilisateurCourant']->_id));
$resultpost = $reqpost -> fetchAll();

//Récupération des informations utilisateur
$info = $dbh->prepare("SELECT * FROM infosuser WHERE iduser = ?");
$info -> execute(array($_SESSION['UtilisateurCourant']->_id));
$information = $info->fetch();

//Récupération de l'expérience utilisateur
$reqexp = $dbh->prepare("SELECT * FROM job WHERE userid = ? ORDER BY datededebut DESC ");
$reqexp->execute(array($_SESSION['UtilisateurCourant']->_id));
$resultexp = $reqexp->fetch();

//Récupération des projets utilisateur
$reqprojets = $dbh->prepare("SELECT titre, description, image FROM projets WHERE userid = ? ORDER BY id DESC" );
$reqprojets -> execute(array($_SESSION['UtilisateurCourant']->_id));
$resultprojets = $reqprojets -> fetch();

//Récupération  des liens pour les réseaux sociaux utilisateur
$reqlink = $dbh->prepare("SELECT * FROM sociallink WHERE userid = ?" );
$reqlink -> execute(array($_SESSION['UtilisateurCourant']->_id));
$resultlink = $reqlink -> fetch();

SetPost($dbh);

?>
<div id="acceuil">
    <div class="filactus">

        <!--                                Post-->
        <div id="myPost">
            <div id="tablettePost">
                <img id="photopost" src="profilpicture/<?php echo $reqpict['avatar'] ?>">
                <form class="post" method="post" action="">
                    <input type="text" name="post" id="post" placeholder="Exprimez vous!">
                    <input id="img" type="image" src="maquettes/publier.svg" alt="soumettre">
                </form  >
            </div>
        </div>
        <ul class="publication">
        <?php foreach ($resultpost as $post) { ?>
            <li>
                <div class="namepost">
                    <img src="profilpicture/<?php echo $post['avatar'] ?>">
                    <h4>
                    <?php echo $post['username'] ?>
                    </h4>
                    <div class="datepublication">
                        <?php echo $post ['datecreation'] ?>
                    </div>
                </div>
                <div class="contenuPost">
                    <?php echo $post['post'] ?>
                </div>
                <div id="signalPost">
                    <a href="#">Signaler</a>
                </div></li>
        <?php } ?>
    </ul>
    </div>
    <div class="colonnegauche">
        <div class="recapInfosAcceuil">
            <h2><?php echo $information['prenom']?> <?php echo $information['nom']?></h2>
            <?php
                if (isset($resultexp['poste'])){
            ?>
            <p><?php echo $resultexp['poste']; ?></p>
            <?php
                }
            if (isset($resultexp['entreprise'])){
            ?>
            <p><?php echo $resultexp['entreprise']?></p>
            <?php
            }
            ?>
        </div>
        <div class="recapProjetAcceuil">
            <h2>Dernier projet ajouté:</h2>
            <?php
                if (!empty($resultprojets)) {
            ?>
                <div class="imageprojetAcceuil">
                    <img src="projectpicture/<?php echo $resultprojets['image']?>">
                </div>
                <div class="titreProjetAcceuil"><?php
                    echo $resultprojets['titre'] ?>
                </div>
                <br>
                <div class="descriptionProjetAcceuil">
                    <?php
                    echo $resultprojets['description']
                    ?>
                </div>
            <?php
            }
            ?>
        </div>
        <div class="recapSocialLinkAcceuil">
            <h2>Réseaux Sociaux</h2>
            <?php
            if (!empty($resultlink['facebook'])) {
                ?>
                <div class="fcbAcceuil">
                    <a href="<?php echo $resultlink['facebook']?>"><img src="maquettes/facebook-f.svg"></a>
                </div>
                <?php
            }
            if (!empty($resultlink['tweeter'])) {
            ?>
                <div class="twitAcceuil">
                    <a href="<?php echo $resultlink['tweeter']?>"><img src="maquettes/twitter.svg"></a>
                </div>
                <?php
            }
            if (!empty($resultlink['instagram'])) {
            ?>
                <div class="instaAcceuil">
                    <a href="<?php echo $resultlink['instagram']?>"><img src="maquettes/instagram.svg"></a>
                </div>
                <?php
            }
            if (!empty($resultlink['linkedin'])) {
            ?>
            <div class="linkAcceuil">
                <a href="<?php echo $resultlink['linkedin']?>"><img src="maquettes/linkedin-in.svg"></a>
            </div>
            <?php
            }
            if (!empty($resultlink['gitlab'])) {
            ?>
            <div class="gitAcceuil">
                <a href="<?php echo $resultlink['gitlab']?>"><img src="maquettes/gitlab.svg"></a>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</div>